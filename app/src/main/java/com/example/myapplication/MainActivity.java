package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button iAmAButton = findViewById(R.id.button);

        iAmAButton.setOnClickListener(view -> iAmAButton.setText("Clicked"));

        playMusic();
    }

    // Methode für Musik
    private void playMusic() {
        MediaPlayer mainmusic = MediaPlayer.create(MainActivity.this, R.raw.mainmusic);
        mainmusic.start();
    }
}